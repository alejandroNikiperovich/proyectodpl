// Configuración de Passport para sesiones y cookies
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const User = require("../models/User");

// Strategia local para passport
passport.use(new LocalStrategy(
    function(email, password, done) {
        // Encuentra un usuario según su email
        User.findOne({email:email}, function(err, user) {
            if (err) return done(err);

            // Error si no encuentra el usuario
            if (!user) return done(null, false, {message: "Email is incorrect or doesn't exist"});

            // Error si la contraseña es incorrecta
            if (!user.validPassword(password)) return done(null, false, {message: "Incorrect password"});

            return done(null, user);
        });
    }
));

// Guarda el id del usuario en la sesión
passport.serializeUser(function(user, cb) {
    cb(null, user.id);
});

// Elimina el id del usuario en la sesión
passport.deserializeUser(function(id, cb) {
    User.findById(id, function(err, user) {
        cb(err, user);
    });
});

module.exports = passport;