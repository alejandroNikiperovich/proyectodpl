// Require
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const passport = require("./config/passport");
const session = require("express-session");

// Require rutas
const indexRouter = require('./routes/index');
const userRouter = require("./routes/users");
const tokenRouter = require("./routes/tokens");
const planRouter = require("./routes/plans");
const registerRouter = require("./routes/register");

const planAPIRouter = require("./routes/api/plans");
const contractAPIRouter = require("./routes/api/contracts");
const userAPIRouter = require("./routes/api/users");
const authAPIRouter = require("./routes/api/auth");

// Iniciamos Express y creamos un espacio en memoria para las sesiones
let app = express();
const store = new session.MemoryStore;
const User = require("./models/User");
const Token = require("./models/Token");

// Conexión con mongodb
mongoose.connect("mongodb://localhost/proyectoDPL", {
  useUnifiedTopology: true, 
  useNewUrlParser: true, 
  useCreateIndex: true,
  useFindAndModify: false
});

mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on("error", console.error.bind("Error de conexión con MongoDB"));

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.set("secretkey", "JWT_PWD_!!223344");

// Creamos una sesión
app.use(session({
  cookie: {maxAge: 240*60*60*1000},
  store: store,
  saveUninitialized: true,
  resave: "true",
  secret: "cualquier cosa no pasa nada 477447"
}))

// Middleware
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

// Rutas CRUD
app.use('/', indexRouter);
app.use("/register", registerRouter);
app.use("/users", loggedIn, userRouter);
app.use("/token", tokenRouter);
app.use("/services", planRouter);

// Rutas API
app.use("/api/plans", planAPIRouter);
app.use("/api/contracts", validateUser, contractAPIRouter);
app.use("/api/users", userAPIRouter);
app.use("/api/auth", authAPIRouter);

// Rutas para loguearse
app.get("/login", function(req, res) {
  res.render("session/login");
});

app.post("/login", function(req, res, next) {
  passport.authenticate("local", function(err, user, info) {
    if (err) return next(err);
    if (!user) return res.render("session/login", {info});

    req.logIn(user, function (err) {
      if (err) return next(err);
      
      return res.redirect("/");
    });
  })(req, res, next);
});

// Rutas para cerrar sesión
app.get("/logout", function (req, res) {
  req.logOut();
  res.redirect("/");
});

// Rutas para resetear la contraseña
app.get("/forgotPassword", function(req, res) {
  res.render("session/forgotPassword");
});

app.post("/forgotPassword", function(req, res) {
  User.findOne({email: req.body.email}, function(err, user) {
    if (!user) return res.render("session/forgotPassword", {info: {message: "Email doesn't exist."}});

    user.resetPassword(function(err) {
      if (err) return next(err);

      res.render("session/forgotPasswordMessage");
    });
  });
});

app.get("/resetPassword/:token", function(req, res, next) {
  Token.findOne({token: req.params.token}, function(err, token) {
    if (!token) return res.status(400).send({type: "not-verified", msg: "No user found."});

    User.findById(token._userId, function(err, user) {
      if (!user) return res.status(400).send({msg: "No user found."});

      res.render("session/resetPassword", {errors: {}, user: user});
    });
  });
});

app.post("/resetPassword", function(req, res) {
  if (req.body.password != req.body.confirmPassword) {
    res.render("session/resetPassword", {errors: {confirmPassword: {message: "Passwords don't match."}},
    user: new User({email: req.body.email})});
    return;
  }

  User.findOne({email: req.body.email}, function(err, user) {
    user.password = req.body.password,
    user.save(function(err) {
      if (err) {
        res.render("session/resetPassword", {errors: err.errors, user: new User({email: req.body.email})});
      } else {
        res.redirect("/login");
      }
    });
  });
});

// Catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// Error handler
app.use(function(err, req, res, next) {
  // Set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // Render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Función para requerir que haya un usuario logueado en CRUD
function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log("Not logged in");
    res.redirect("/login");
  }
}

// Función para validar un usuario en la API
function validateUser(req, res, next) {
  jwt.verify(req.headers["x-access-token"], req.app.get("secretkey"), function(err, decoded) {
    if (err) {
      res.json({status: "error", message: err.message, data: null});
    } else {
      req.body.userId = decoded.id;
      console.log("jwt verify: " + decoded);
      next();
    }
  });
}

module.exports = app;