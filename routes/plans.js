// Router Plan CRUD
const express = require("express");
const router = express.Router();
const planController = require("../controllers/plan");

router.get("/", planController.listPlans);

module.exports = router;