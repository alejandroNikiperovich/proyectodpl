// Router User CRUD
const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");

router.get("/", userController.list);
router.get("/:id/update", userController.getUpdate);
router.post("/:id/update", userController.postUpdate);
router.post("/:id/delete", userController.delete);

module.exports = router;