// Router para registrar un nuevo usuario en CRUD
// Aparte de User para que no requira estar logueado
const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");

router.get("/", userController.getRegister);
router.post("/", userController.postRegister);

module.exports = router;