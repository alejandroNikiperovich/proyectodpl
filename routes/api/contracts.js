// Router Contract API
const express = require("express");
const router = express.Router();
const contractAPI = require("../../controllers/api/contractAPI");

router.get("/", contractAPI.listContracts);
router.post("/create", contractAPI.createContract);
router.delete("/delete", contractAPI.deleteContract);
router.put("/put", contractAPI.updateContract);

module.exports = router;