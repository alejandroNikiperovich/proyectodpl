// Router autenticación API
const express = require("express");
const router = express.Router();
const authAPI = require("../../controllers/api/authAPI");

router.post("/authenticate", authAPI.authenticate);

module.exports = router;