// Router Plan API
const express = require("express");
const router = express.Router();
const planAPI = require("../../controllers/api/planAPI");

router.get("/", planAPI.listPlans);
router.post("/create", planAPI.createPlan);
router.delete("/delete", planAPI.deletePlan);
router.put("/put", planAPI.updatePlan);

module.exports = router;