// Router User API
const express = require("express");
const router = express.Router();
const userAPI = require("../../controllers/api/userAPI");

router.get("/", userAPI.listUsers);
router.post("/create", userAPI.createUser);
router.delete("/delete", userAPI.deleteUser);
router.put("/put", userAPI.updateUser);

module.exports = router;