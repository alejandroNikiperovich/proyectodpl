// Controlador modelo USER CRUD
let User = require("../models/User");

// Registrar un User (crear uno nuevo)
exports.getRegister = function(req, res) {
    res.render("register", {errors:{}, user: new User()});
}

exports.postRegister = function(req, res) {
    // Comprueba que el campo confirmar password es igual que la password
    if (req.body.password != req.body.confirmPassword) {
        res.render("register", 
            {errors: 
                {confirm_password: {message: "Passwords don't match"}}, 
                user: new User({username: req.body.username, email: req.body.email})
            });

        return;
    }

    // Crea el usuario a partir de los datos del formulario
    User.create({username:req.body.username, email: req.body.email, password: req.body.password}, function(err, newUser) {
        if(err){
            res.render("register", {errors: err.errors, user: new User({username: req.body.username, email: req.body.email})});
        } else {
            newUser.sendNewUserMail();
            res.redirect("/login");
        }
    });
}

// Devuelve todos los User de la BBDD
exports.list = function(req, res, next){     
    User.find({}, function(err,users) {
        if(err) res.send(500, err.message);
        res.render("users/index", {users: users});
    });
}

// Devuelve el usuario con la id de la URL a la vista update
exports.getUpdate = function(req, res, next) {
    User.findById(req.params.id, function (err, user) {
        res.render("users/update", {errors:{}, user: user});
    });
}

// Actualiza el user con la id que se pasa por URL
exports.postUpdate = function(req, res, next){
    let update_values = {username: req.body.username};
    User.findByIdAndUpdate(req.params.id, update_values, function(err, user) {
        // Si hay errores se envía a la misma vista un user nuevo con el nombre y email nuevos y los errores
        if (err) {
            console.log(err);
            res.render("users/update", {errors: err.errors, user: new User({username: req.body.username, email: req.body.email})});
        } else {
            res.redirect("/users");
            return;
        }
    });
}

// Elimina un User
exports.delete = function(req, res, next){
    User.findByIdAndDelete(req.body.id, function(err){
        if(err)
            next(err);
        else   
            res.redirect("/users");
    });
}