// Controlador modelo Plan CRUD
let Plan = require("../models/Plan");

// Devuelve todos los Plan de la BBDD
exports.listPlans = function(req, res) {
    Plan.list(function (err, plans) {
        if (err) res.send(err.message);

        res.render("services", {plans: plans});
    });
};