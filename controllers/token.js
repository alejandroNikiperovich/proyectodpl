// Controlador modelo Token
let Usuario = require("../models/User");
let Token = require("../models/Token");

module.exports = {
    // Verifica un User mediante un Token
    confirmationGet: function(req, res, next) {
        // Encuentra un Token según la URL
        Token.findOne({token: req.params.token}, function (err, token) {
            // Error si no encuentra el Token
            if (!token) return res.status(400).send({
                type: "not-verified",
                msg: "Email hasn't been verified"
            });

            // Encuentra el User que está relacionado con el Token
            Usuario.findById(token._userId, function (err, user) {
                // Error si no encuentra el User
                if (!user) return res.status(400).send({
                    msg: "User not found"
                });

                if (user.verified) return res.redirect("/");

                // Verifica el User
                user.verified = true;

                user.save(function(err) {
                    if (err) {return res.status(500).send({msg: err.message})}

                    res.redirect("/");
                });
            });
        });
    }
}