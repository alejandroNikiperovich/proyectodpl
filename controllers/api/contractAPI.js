// Controlador modelo Contract API
// Requirere estar autenticado para manipularse
const Contract = require("../../models/Contract");

// Devuelve todos los Contract de la BBDD
exports.listContracts = function(req, res) {
    Contract.list(function(err, contracts) {
        if (err) res.status(500).send(err.message);

        res.status(200).json ({
            contracts: contracts
        });
    })
};

// Crea un Contract nuevo
exports.createContract = function(req, res) {
    let contract = new Contract({
        from: req.body.from,
        to: req.body.to,
        user: req.body.idUser,
        plan: req.body.idPlan
    });

    Contract.add(contract, function (err, newContract) {
        if (err) res.status(500).send(err.message);

        res.status(201).send(newContract);
    });
};

// Elimina un Contract
exports.deleteContract = function(req, res) {
    let idContract = req.body.id;
    Contract.removeById(idContract, function (err, contract) {
        if (err) res.status(500).send(err.message);

        res.status(204).send();
    });
};

// Modifica un Contract
exports.updateContract = function(req, res) {
    let idContract = req.body.id;

    Contract.update(idContract, req.body, function (err, contract) {
        if (err) res.status(500).send(err.message);

        contract = req.body;
        res.status(200).json ({
            Contracts: contract
        });
    });
};