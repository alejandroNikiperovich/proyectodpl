// Controlador modelo User API
const bcrypt = require("bcrypt");
const User = require("../../models/User");

// Devuelve todos los User de la BBDD
exports.listUsers = function(req, res) {
    User.list(function(err, users) {
        if (err) res.status(500).send(err.message);

        res.status(200).json ({
            users: users
        });
    })
};

// Crea un User
exports.createUser = function(req, res) {
    let user = new User({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    });

    User.add(user, function (err, newUser) {
        if (err) res.status(500).send(err.message);

        res.status(201).send(newUser);
    });
};

// Elimina un User
exports.deleteUser = function(req, res) {
    let idUser = req.body.id;
    User.removeById(idUser, function (err, user) {
        if (err) res.status(500).send(err.message);

        res.status(204).send();
    });
};

// Modifica un User
exports.updateUser = function(req, res) {
    // Encriptamos la contraseña antes de insertarla
    let password = bcrypt.hashSync(req.body.password, 10);
    let idUser = req.body.id;

    User.update(idUser, {
        username: req.body.username,
        email: req.body.email,
        password: password
    },
    function (err, user) {
        if (err) res.status(500).send(err.message);

        res.status(200).json ({
            idUser: idUser
        });
    });
};