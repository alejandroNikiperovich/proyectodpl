// Controlador modelo Plan API
const Plan = require("../../models/Plan");

// Devuelve todos los Plan de la BBDD
exports.listPlans = function(req, res) {
    Plan.list(function(err, plans) {
        if (err) res.status(500).send(err.message);

        res.status(200).json ({
            plans: plans
        });
    })
};

// Crea un Plan
exports.createPlan = function(req, res) {
    let plan = new Plan({
        name: req.body.name,
        price: req.body.price,
        months: req.body.months
    });

    Plan.add(plan, function (err, newPlan) {
        if (err) res.status(500).send(err.message);

        res.status(201).send(newPlan);
    });
};

// Elimina un Plan
exports.deletePlan = function(req, res) {
    let idPlan = req.body.id;
    Plan.removeById(idPlan, function (err, plan) {
        if (err) res.status(500).send(err.message);

        res.status(204).send();
    });
};

// Modifica un Plan
exports.updatePlan = function(req, res) {
    let idPlan = req.body.id;

    Plan.update(idPlan, req.body, function (err, plan) {
        if (err) res.status(500).send(err.message);

        plan = req.body;
        res.status(200).json ({
            plans: plan
        });
    });
};