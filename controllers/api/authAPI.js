// Controlador para autenticarse en la API para poder manipular los contratos
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../../models/User");

module.exports = {
    authenticate: function(req, res, next) {
        // Encuentra un usuario según su email
        User.findOne({email:req.body.email}, function(err, user){
            if (err) {
                next(err);
            } else {
                // Error si no encuentra el usuario
                if (user === null) {
                    return res.status(401).json({status:'error', message: "Incorrect email or password!", data:null});
                }

                // Creamos el token si encontramos al usuario y la contraseña es correcta
                if (user !=null && bcrypt.compareSync(req.body.password, user.password)) {
                    let token = jwt.sign({id: user._id}, req.app.get('secretkey'), {expiresIn: '7d'});
                    res.status(200).json({message: 'User found!', data:{user: user, token: token}});
                } else {
                    // Error por contraseña incorrecta
                    res.status(401).json({status: 'error', message: 'Incorrect email or password!', data:null});
                }
            }
        });
    }
}