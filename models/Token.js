// Modelo Token
const mongoose = require("mongoose");

let Schema = mongoose.Schema;
let TokenSchema = new Schema ({
    // Id del User
    _userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Usuario"
    },

    // El Token de por sí
    token: {
        type: String,
        required: true
    },

    // Fecha de creación y tiempo que tarda en expirar
    createAt: {
        type: Date,
        required: true,
        default: Date.now,
        expires: 43200
    }
});

module.exports = mongoose.model("Token", TokenSchema);