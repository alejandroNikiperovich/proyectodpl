// Modelo Plan
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Nombre y precio del servicio
// Tiempo en meses que dura
let planSchema = new Schema({
    name: String,
    price: String,
    months: Number
});

// Devuelve todos los Plan
planSchema.statics.list = function(cb) {
    return this.find({}, cb);
};

// Crea un Plan
planSchema.statics.add = function(plan, cb) {
    return this.create(plan, cb);
};

// Encuentra un Plan según su id
planSchema.statics.findById = function(idPlan, cb) {
    return this.findOne({_id: idPlan}, cb);
};

// Elimina un Plan
planSchema.statics.removeById = function(idPlan, cb) {
    return this.findOneAndDelete({_id: idPlan}, cb);
};

// Modifica un Plan
planSchema.statics.update = function(idPlan, changes, cb) {
    return this.findOneAndUpdate({_id: idPlan}, changes, cb);
};

module.exports = mongoose.model("Plan", planSchema);