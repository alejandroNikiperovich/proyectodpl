// Modelo User
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const uniqueValidator = require("mongoose-unique-validator");
const crypto = require("crypto");
const Schema = mongoose.Schema;

const Token = require("./Token");
const mailer = require("../mailer/mailer");

let saltRounds = 10;

// Valida "email" segun el regexp "re"
let validateEmail = function(email) {
    let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/;
    return re.test(email);
}

let userSchema = new Schema({
    // Nombre de usuario
    username: {
        type: String,
        trim: true,
        required: [true, "Username required"],
    },

    // Email
    email: {
        type: String,
        trim: true,
        required: [true, "Email required"],
        lowercase: true,
        unique: true,
        validate: [validateEmail, "Please, insert a valid email"],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/]
    },

    // Contraseña
    password: {
        type: String,
        required: [true, "Password required"]
    },

    passwordResetToken: String,

    passwordResetTokenExpires: Date,

    verified: {
        type: Boolean,
        default: false
    }
});

userSchema.plugin(uniqueValidator, {message: "Email already exists"});

// Encriptamos la contraseña antes de guardarla
userSchema.pre("save", function(next) {
    if (this.isModified("password")) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }

    next();
});

// Devuelve todos los usuarios
userSchema.statics.list = function(cb) {
    return this.find({}, cb);
};

// Crea un User
userSchema.statics.add = function(user, cb) {
    return this.create(user, cb);
};

// Encuentra un User según su id
userSchema.statics.findById = function(idUser, cb) {
    return this.findOne({_id: idUser}, cb);
};

// Elimina un User
userSchema.statics.removeById = function(idUser, cb) {
    return this.findOneAndDelete({_id: idUser}, cb);
};

// Modifica un User
userSchema.statics.update = function(idUser, changes, cb) {
    return this.findOneAndUpdate({_id: idUser}, changes, cb);
};

// Comprueba que una "password" sea igual a la contraseña del usuario
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
}

// Envía un email a un User que se acaba de crear con un Token para verificar el email
userSchema.methods.sendNewUserMail = function(cb) {
    // Creamos el Token
    let token = new Token({
        _userId: this.id,
        token: crypto.randomBytes(16).toString("hex")
    });

    // Email de destino (el que se acaba de registrar)
    let to = this.email;

    token.save(function(err) {
        if (err) {
            return console.log(err.message);
        }

        // Montamos el mensaje
        let mailOptions = {
            from: "creative@email.com",
            to: to,
            subject: "Account verification",
            text: "Please, to verify your account click on this link: \n" +
                "http://192.168.0.32:3000" + "\/token/confirmation\/" +
                token.token + ".\n"
        };

        // Comprobamos que funciona nodemailer
        mailer.verify(function(error, success) {
            if (error) {
                console.log(error);
            } else {
                console.log("Server is ready to take our messages")
            }
        });

        // Enviamos el mensaje
        mailer.sendMail(mailOptions, function(err, info) {
            if (err) {
                console.log("Error");
                return console.log(err.message);
            }

            console.log("Email sent to " + to);
        });
    });
};

// Permite a un User cambiar su contraseña mediante un Token
userSchema.methods.resetPassword = function(cb) {
    let token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString("hex")});
    let to = this.email;

    token.save(function(err) {
        if (err) return cb(err);

        let mailOptions = {
            from: "creative@email.com",
            to: to,
            subject: "Password reset",
            text: "Please, to reset your password click on this link: \n" +
                "http://192.168.0.32:3000" + "\/resetPassword\/" +
                token.token + ".\n"
        };

        mailer.sendMail(mailOptions, function(err) {
            if (err) {return cb(err);}

            console.log("Mail sent to " + to);
        });

        cb(null);
    })
}

module.exports = mongoose.model("User", userSchema);