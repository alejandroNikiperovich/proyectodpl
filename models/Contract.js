// Modelo Contract
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Fecha de inicio y fin del contrato
// Relaciona un User con un Plan
let contractSchema = new Schema({
    from: Date,
    to: Date,
    user: {type: Schema.Types.ObjectId, ref: "User"},
    plan: {type: Schema.Types.ObjectId, ref: "Plan"}
});

// Encontrar todos los Contract
contractSchema.statics.list = function(cb) {
    return this.find({}, cb);
};

// Crear un Contract
contractSchema.statics.add = function(contract, cb) {
    return this.create(contract, cb);
};

// Encontrar un Contract según su id
contractSchema.statics.findById = function(idContract, cb) {
    return this.findOne({_id: idContract}, cb);
};

// Eliminar un Contract
contractSchema.statics.removeById = function(idContract, cb) {
    return this.findOneAndDelete({_id: idContract}, cb);
};

// Modificar un Contract
contractSchema.statics.update = function(idContract, changes, cb) {
    return this.findOneAndUpdate({_id: idContract}, changes, cb);
};

module.exports = mongoose.model("Contract", contractSchema);